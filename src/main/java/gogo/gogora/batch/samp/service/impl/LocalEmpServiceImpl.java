package gogo.gogora.batch.samp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import gogo.gogora.batch.samp.service.LocalEmpService;
import gogo.gogora.batch.samp.vo.EmpVo;

@Service("localEmpService")
public class LocalEmpServiceImpl implements LocalEmpService {

	private List<EmpVo> empList;
	private int curIndex;
	
	private String[] enames = {"SMITH", "ALLEN", "WARD", "JONES", "MARTIN", "BLAKE", "CLARK", "SCOTT", "KING", "TURNER", "ADAMS", "JAMES", "FORD", "MILLER"};
	private String[] jobs = {"CLERK", "SALESMAN", "MANAGER", "ANALYST", "ANALYST", "PRESIDENT"};
	
	private Random generator = new Random();
	
	public LocalEmpServiceImpl(){
		curIndex = 0;
		empList = new ArrayList<>();
		for(int i=0; i<10; i++){
			EmpVo empVo = buildEmpVo();
			empVo.setEmpno(i+"");
			empList.add(empVo);
		}
	}
	
	private EmpVo buildEmpVo(){
		EmpVo empVo = new EmpVo();
		empVo.setEname(enames[generator.nextInt(enames.length-1)]);
		empVo.setJob(jobs[generator.nextInt(jobs.length-1)]);
		return empVo;
	}
	
	public EmpVo getRemoteReceiveData(){
		EmpVo empVo = null;
		if(curIndex<empList.size()){
			empVo = empList.get(curIndex);
			curIndex++;
		}
		return empVo;
	}
	

}
