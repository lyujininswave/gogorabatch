package gogo.gogora.batch.samp.service;

import gogo.gogora.batch.samp.vo.EmpVo;

public interface LocalEmpService {

	public EmpVo getRemoteReceiveData() throws Exception;
	
}
