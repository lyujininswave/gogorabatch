package gogo.gogora.batch.samp.dao;

import org.springframework.stereotype.Repository;

import com.inswave.elfw.exception.ElException;

import gogo.gogora.batch.cmmn.GogoraDefaultAbstractDAO;
import gogo.gogora.batch.samp.vo.EmpVo;

/**   
 * @ClassSubJect 데모용 사원정보 관련 처리를 담당하는 DAO
 * @Class Name : EmpDAO.java
 * @Description : 데모용 사원정보 관련 처리를 담당하는 DAO
 * @Modification Information  
 * @
 * @  수정일                  수정자                  수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2019. 08.01   샘플작성자                최초생성
 * 
 * @author EL개발팀
 * @since 2019. 08.01
 * @version 1.0
 * @see
 * 
 *  Copyright Inswave (C) by Sampler All right reserved.
 */
@Repository("emp2DAO")
public class Emp2DAO extends GogoraDefaultAbstractDAO {


    public int insertEmp(EmpVo vo) throws ElException {
        return insert("gogo.gogora.batch.samp.insertEmp2", vo);
    }

}
