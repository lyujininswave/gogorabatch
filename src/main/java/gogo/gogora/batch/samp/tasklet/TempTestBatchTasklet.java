package gogo.gogora.batch.samp.tasklet;

import java.sql.SQLException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;

import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.dao.EmpDAO;
import gogo.gogora.batch.samp.vo.EmpVo;

public class TempTestBatchTasklet implements Tasklet, InitializingBean {
	private SqlSessionTemplate sqlSessionTemplate;
	
	public static Map<String,String> mp = new HashMap<String,String>();
	
	private Map<String,String> parameterValues;
	
	 @Resource(name="empDAO") 
	 private EmpDAO empDAO;   
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
	}
	
	public void setParameterValues(Map<String,String> parameterValues) {
		this.parameterValues = parameterValues;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		if (sqlSessionTemplate == null) {
			sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory, ExecutorType.BATCH);
		}
	}
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		while(!contribution.getStepExecution().isTerminateOnly()){
			// 스텝이 종료되지 않는 한 주기적으로 계속 처리되어야 하는 business 업무 호출(반드시 오류 처리 필요함)
			defferedProcess(parameterValues);
		}		
		
		return RepeatStatus.FINISHED;
	}
	
	public void defferedProcess(Map<String,String> parameterValues) {

		try {
			String paramEname = parameterValues.get("ename");
			if(paramEname==null || paramEname.equals("")){
				paramEname="홍길동";
			}
			
			String paramPeriod = parameterValues.get("period");
			if(paramPeriod==null || paramPeriod.equals("")){
				paramPeriod="1000";
			}
			
			System.out.println("-paramEname:::" + paramEname);
			System.out.println("-paramPeriod:::" + paramPeriod);
			
			SqlSession sqlSession = empDAO.getSqlBatchSession();
			sqlSession.getConnection().setAutoCommit(false);
			
			EmpVo empVo = new EmpVo();
			empVo.setEname(paramEname);
			
			long loCurDate = System.currentTimeMillis();
			Date dt = new Date();
	    	dt.setTime(loCurDate);
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			empVo.setHiredate(sdf.format(dt));
			
			sqlSessionTemplate.update("gogo.gogora.batch.samp.updateEmp", empVo);
			
			Cursor<EmpVo> cursor = sqlSessionTemplate.selectCursor("selectEmpList", empVo);
			for( EmpVo ep : cursor ) {
				System.out.println("empVo==>>" + ep);
			}
			
			Thread.sleep(Integer.parseInt(paramPeriod));
			 
		} catch (SQLException sqle){
			AppLog.error(" - defferedProcess SQLException 오류", sqle);
		} catch (InterruptedException ie) {
			AppLog.error(" - defferedProcess InterruptedException 오류", ie);
		} catch (Exception e){
			AppLog.error(" - defferedProcess Exception 오류", e);
		}
	}

}
