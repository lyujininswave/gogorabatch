package gogo.gogora.batch.samp.partition;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.dao.EmpDAO;
import gogo.gogora.batch.samp.vo.EmpVo;
 
public class EmpRangePartitioner implements Partitioner {
  
	 @Resource(name="empDAO") 
	 private EmpDAO empDAO;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		long startRow = 1;
		EmpVo empVo = new EmpVo();
		long totalRow = empDAO.selectEmpListTotCnt(empVo);
		AppLog.debug("- totalRow:" + totalRow );
		
		long targetSize = (totalRow - startRow) / gridSize + 1;
		AppLog.debug("- targetSize:" + targetSize );
		
		Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>(); 
		long number = 0;
		long start = startRow;
		long end = start + targetSize - 1;
		
		AppLog.debug("- start:" + start );
		AppLog.debug("- end:" + end );
		
		while (start <= totalRow) {
			if (end >= totalRow) {
				end = totalRow;
			}
			ExecutionContext value = new ExecutionContext();
			AppLog.debug("- minValue:" + start );
			AppLog.debug("- maxValue:" + end );
			
			value.putLong("minValue", start);
			value.putLong("maxValue", end);
			result.put("partition" + number, value);

			start += targetSize;
			end += targetSize;
			number++;
		}
		return result;
	}

}
