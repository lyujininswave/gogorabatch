package gogo.gogora.batch.samp.vo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.inswave.elfw.annotation.ElDto;
import com.inswave.elfw.annotation.ElDtoField;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.inswave.elfw.log.AppLog;

@JsonFilter("elExcludeFilter")
@ElDto(FldYn = "Y", delimeterYn = "Y", logicalName = "헤더VO")
public class EmpHeadVo extends com.inswave.elfw.core.BatchHeadVO {

    private int _offset;

    public EmpHeadVo(){
        this._offset = 0;
    }

    public EmpHeadVo(int iOffset){
        this._offset = iOffset;
    }

    @ElDtoField(logicalName = "처리구분", physicalName = "prcGbn", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private String prcGbn;

    @ElDtoField(logicalName = "본문카운트", physicalName = "bodyCount", type = "long", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private long bodyCount;

    public String getPrcGbn(){
        return prcGbn;
    }

    public void setPrcGbn(String prcGbn){
        this.prcGbn = prcGbn;
    }

    public long getBodyCount(){
        return bodyCount;
    }

    public void setBodyCount(long bodyCount){
        this.bodyCount = bodyCount;
    }

    @Override
    public String toString() {
        return "EmpHeadVo [prcGbn=" + prcGbn + ",bodyCount=" + bodyCount + "]";
    }

    public boolean isFixedLengthVo() {
        return true;
    }

    public byte[] marshalFld() throws Exception{
        return marshalFld( com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

	public byte[] marshalFld(String encode) throws Exception{
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(bout);
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.prcGbn , 10, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.longToBytes(this.bodyCount , 10) );
        } catch (Exception e) {
                AppLog.error("marshalFld Error:["+ toString()+"]", e);
                throw e;
        } finally {
            try	{
                if (out != null) out.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld out close Error", ie);
           }
            try	{
                if (bout != null) bout.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld bout close Error", ie);
           }
        }
        return bout.toByteArray();
    }

    public void unMarshalFld( byte[] bytes ) throws Exception{
        unMarshalFld( bytes, com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

    public void unMarshalFld( byte[] bytes , String encode) throws Exception{
        try{ 
            this.prcGbn = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 10, encode );
             _offset += 10;
             this.bodyCount = com.inswave.elfw.util.TypeConversionUtil.bytesToLong( bytes, _offset, 10, encode );
             _offset += 10;
        }catch(Exception e) { 
            String errorLine = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, 0, bytes.length, encode );
            AppLog.error("unMarshalFld Error:["+ errorLine+"]", e);
            throw e;
        } 
    }

    public int getOffset(){
        return _offset;
    }

    public String marshalDelimeter( String delimterGbn ) throws Exception{
        return marshalDelimeter( delimterGbn, null );
    }

    public String marshalDelimeter( String delimterGbn, String encode ) throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append( this.prcGbn == null ? "": prcGbn ).append(delimterGbn);
        sb.append( this.bodyCount ).append(delimterGbn);
        return sb.toString().substring(0, sb.toString().length()-delimterGbn.length());
    }

    public void unMarshalDelimeter( String lineStr, String delimterGbn ) throws Exception{
        unMarshalDelimeter( lineStr, delimterGbn, null ); 
    }

    public void unMarshalDelimeter( String lineStr ,String delimterGbn, String encode) throws Exception{
        String fieldDatas[] = lineStr.split( delimterGbn );
        try{
            this.prcGbn = fieldDatas[_offset];
            _offset ++;
            this.bodyCount = com.inswave.elfw.util.TypeConversionUtil.parseToLong( fieldDatas[_offset] );
            _offset ++;
        }catch(Exception e) {
            AppLog.error("unMarshalDelimeter Error:["+ lineStr+"]", e);
            throw e;
        }
    }

    @Override
    public void _xStreamEnc() {
    }


    @Override
    public void _xStreamDec() {
    }


}
