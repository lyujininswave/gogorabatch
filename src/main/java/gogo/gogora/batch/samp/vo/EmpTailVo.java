package gogo.gogora.batch.samp.vo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.inswave.elfw.annotation.ElDto;
import com.inswave.elfw.annotation.ElDtoField;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.inswave.elfw.log.AppLog;

@JsonFilter("elExcludeFilter")
@ElDto(FldYn = "Y", delimeterYn = "Y", logicalName = "테일VO")
public class EmpTailVo extends com.inswave.elfw.core.BatchTailVO {

    private int _offset;

    public EmpTailVo(){
        this._offset = 0;
    }

    public EmpTailVo(int iOffset){
        this._offset = iOffset;
    }

    @ElDtoField(logicalName = "구분", physicalName = "gbn", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private String gbn;

    @ElDtoField(logicalName = "본문카운트", physicalName = "bodyCount", type = "long", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private long bodyCount;

    @ElDtoField(logicalName = "합계", physicalName = "sumAccount", type = "long", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private long sumAccount;

    public String getGbn(){
        return gbn;
    }

    public void setGbn(String gbn){
        this.gbn = gbn;
    }

    public long getBodyCount(){
        return bodyCount;
    }

    public void setBodyCount(long bodyCount){
        this.bodyCount = bodyCount;
    }

    public long getSumAccount(){
        return sumAccount;
    }

    public void setSumAccount(long sumAccount){
        this.sumAccount = sumAccount;
    }

    @Override
    public String toString() {
        return "EmpTailVo [gbn=" + gbn + ",bodyCount=" + bodyCount + ",sumAccount=" + sumAccount + "]";
    }

    public boolean isFixedLengthVo() {
        return true;
    }

    public byte[] marshalFld() throws Exception{
        return marshalFld( com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

	public byte[] marshalFld(String encode) throws Exception{
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(bout);
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.gbn , 10, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.longToBytes(this.bodyCount , 10) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.longToBytes(this.sumAccount , 10) );
        } catch (Exception e) {
                AppLog.error("marshalFld Error:["+ toString()+"]", e);
                throw e;
        } finally {
            try	{
                if (out != null) out.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld out close Error", ie);
           }
            try	{
                if (bout != null) bout.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld bout close Error", ie);
           }
        }
        return bout.toByteArray();
    }

    public void unMarshalFld( byte[] bytes ) throws Exception{
        unMarshalFld( bytes, com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

    public void unMarshalFld( byte[] bytes , String encode) throws Exception{
        try{ 
            this.gbn = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 10, encode );
             _offset += 10;
             this.bodyCount = com.inswave.elfw.util.TypeConversionUtil.bytesToLong( bytes, _offset, 10, encode );
             _offset += 10;
             this.sumAccount = com.inswave.elfw.util.TypeConversionUtil.bytesToLong( bytes, _offset, 10, encode );
             _offset += 10;
        }catch(Exception e) { 
            String errorLine = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, 0, bytes.length, encode );
            AppLog.error("unMarshalFld Error:["+ errorLine+"]", e);
            throw e;
        } 
    }

    public int getOffset(){
        return _offset;
    }

    public String marshalDelimeter( String delimterGbn ) throws Exception{
        return marshalDelimeter( delimterGbn, null );
    }

    public String marshalDelimeter( String delimterGbn, String encode ) throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append( this.gbn == null ? "": gbn ).append(delimterGbn);
        sb.append( this.bodyCount ).append(delimterGbn);
        sb.append( this.sumAccount ).append(delimterGbn);
        return sb.toString().substring(0, sb.toString().length()-delimterGbn.length());
    }

    public void unMarshalDelimeter( String lineStr, String delimterGbn ) throws Exception{
        unMarshalDelimeter( lineStr, delimterGbn, null ); 
    }

    public void unMarshalDelimeter( String lineStr ,String delimterGbn, String encode) throws Exception{
        String fieldDatas[] = lineStr.split( delimterGbn );
        try{
            this.gbn = fieldDatas[_offset];
            _offset ++;
            this.bodyCount = com.inswave.elfw.util.TypeConversionUtil.parseToLong( fieldDatas[_offset] );
            _offset ++;
            this.sumAccount = com.inswave.elfw.util.TypeConversionUtil.parseToLong( fieldDatas[_offset] );
            _offset ++;
        }catch(Exception e) {
            AppLog.error("unMarshalDelimeter Error:["+ lineStr+"]", e);
            throw e;
        }
    }

    @Override
    public void _xStreamEnc() {
    }


    @Override
    public void _xStreamDec() {
    }


}
