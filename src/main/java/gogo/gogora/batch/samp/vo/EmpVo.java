package gogo.gogora.batch.samp.vo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.inswave.elfw.annotation.ElDto;
import com.inswave.elfw.annotation.ElDtoField;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.inswave.elfw.log.AppLog;

@JsonFilter("elExcludeFilter")
@ElDto(FldYn = "Y", delimeterYn = "Y", logicalName = "데모사원정보 ")
public class EmpVo extends com.inswave.elfw.core.BatchVO {

    private int _offset;

    public EmpVo(){
        this._offset = 0;
    }

    public EmpVo(int iOffset){
        this._offset = iOffset;
    }

    @ElDtoField(logicalName = "사원번호", physicalName = "empno", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 4, dotLen = 0, baseValue = "", desc = "")
    private String empno;

    @ElDtoField(logicalName = "사원명", physicalName = "ename", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private String ename;

    @ElDtoField(logicalName = "직업", physicalName = "job", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 9, dotLen = 0, baseValue = "", desc = "")
    private String job;

    @ElDtoField(logicalName = "직속상사번호", physicalName = "mgr", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 4, dotLen = 0, baseValue = "", desc = "")
    private String mgr;

    @ElDtoField(logicalName = "입사일", physicalName = "hiredate", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private String hiredate;

    @ElDtoField(logicalName = "급여", physicalName = "sal", type = "long", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 0, baseValue = "", desc = "")
    private long sal;

    @ElDtoField(logicalName = "상여", physicalName = "comm", type = "double", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 10, dotLen = 3, baseValue = "", desc = "")
    private double comm;

    @ElDtoField(logicalName = "사원설명", physicalName = "account", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 40, dotLen = 0, baseValue = "", desc = "")
    private String account;

    @ElDtoField(logicalName = "부서번호", physicalName = "deptno", type = "String", typeKind = "", fldYn = "Yes", delimeterYn = "Yes", cryptoGbn = "", length = 2, dotLen = 0, baseValue = "", desc = "")
    private String deptno;

    public String getEmpno(){
        return empno;
    }

    public void setEmpno(String empno){
        this.empno = empno;
    }

    public String getEname(){
        return ename;
    }

    public void setEname(String ename){
        this.ename = ename;
    }

    public String getJob(){
        return job;
    }

    public void setJob(String job){
        this.job = job;
    }

    public String getMgr(){
        return mgr;
    }

    public void setMgr(String mgr){
        this.mgr = mgr;
    }

    public String getHiredate(){
        return hiredate;
    }

    public void setHiredate(String hiredate){
        this.hiredate = hiredate;
    }

    public long getSal(){
        return sal;
    }

    public void setSal(long sal){
        this.sal = sal;
    }

    public double getComm(){
        return comm;
    }

    public void setComm(double comm){
        this.comm = comm;
    }

    public String getAccount(){
        return account;
    }

    public void setAccount(String account){
        this.account = account;
    }

    public String getDeptno(){
        return deptno;
    }

    public void setDeptno(String deptno){
        this.deptno = deptno;
    }

    @Override
    public String toString() {
        return "EmpVo [empno=" + empno + ",ename=" + ename + ",job=" + job + ",mgr=" + mgr + ",hiredate=" + hiredate + ",sal=" + sal + ",comm=" + comm + ",account=" + account + ",deptno=" + deptno + "]";
    }

    public boolean isFixedLengthVo() {
        return true;
    }

    public byte[] marshalFld() throws Exception{
        return marshalFld( com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

	public byte[] marshalFld(String encode) throws Exception{
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(bout);
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.empno , 4, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.ename , 10, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.job , 9, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.mgr , 4, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.hiredate , 10, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.longToBytes(this.sal , 10) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.doubleToBytes(this.comm , 10, 3) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.account , 40, encode ) );
            out.write( com.inswave.elfw.util.TypeConversionUtil.strToSpBytes(this.deptno , 2, encode ) );
        } catch (Exception e) {
                AppLog.error("marshalFld Error:["+ toString()+"]", e);
                throw e;
        } finally {
            try	{
                if (out != null) out.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld out close Error", ie);
           }
            try	{
                if (bout != null) bout.close();
           } catch (IOException ie) {
                AppLog.error("marshalFld bout close Error", ie);
           }
        }
        return bout.toByteArray();
    }

    public void unMarshalFld( byte[] bytes ) throws Exception{
        unMarshalFld( bytes, com.inswave.elfw.ElConfig.getFldEncode() ); 
    }

    public void unMarshalFld( byte[] bytes , String encode) throws Exception{
        try{ 
            this.empno = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 4, encode );
             _offset += 4;
            this.ename = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 10, encode );
             _offset += 10;
            this.job = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 9, encode );
             _offset += 9;
            this.mgr = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 4, encode );
             _offset += 4;
            this.hiredate = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 10, encode );
             _offset += 10;
             this.sal = com.inswave.elfw.util.TypeConversionUtil.bytesToLong( bytes, _offset, 10, encode );
             _offset += 10;
             this.comm = com.inswave.elfw.util.TypeConversionUtil.bytesToDouble( bytes, _offset, 10, encode );
             _offset += 10;
            this.account = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 40, encode );
             _offset += 40;
            this.deptno = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, _offset, 2, encode );
             _offset += 2;
        }catch(Exception e) { 
            String errorLine = com.inswave.elfw.util.TypeConversionUtil.getTrimmedString( bytes, 0, bytes.length, encode );
            AppLog.error("unMarshalFld Error:["+ errorLine+"]", e);
            throw e;
        } 
    }

    public int getOffset(){
        return _offset;
    }

    public String marshalDelimeter( String delimterGbn ) throws Exception{
        return marshalDelimeter( delimterGbn, null );
    }

    public String marshalDelimeter( String delimterGbn, String encode ) throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append( this.empno == null ? "": empno ).append(delimterGbn);
        sb.append( this.ename == null ? "": ename ).append(delimterGbn);
        sb.append( this.job == null ? "": job ).append(delimterGbn);
        sb.append( this.mgr == null ? "": mgr ).append(delimterGbn);
        sb.append( this.hiredate == null ? "": hiredate ).append(delimterGbn);
        sb.append( this.sal ).append(delimterGbn);
        sb.append( this.comm ).append(delimterGbn);
        sb.append( this.account == null ? "": account ).append(delimterGbn);
        sb.append( this.deptno == null ? "": deptno ).append(delimterGbn);
        return sb.toString().substring(0, sb.toString().length()-delimterGbn.length());
    }

    public void unMarshalDelimeter( String lineStr, String delimterGbn ) throws Exception{
        unMarshalDelimeter( lineStr, delimterGbn, null ); 
    }

    public void unMarshalDelimeter( String lineStr ,String delimterGbn, String encode) throws Exception{
        String fieldDatas[] = lineStr.split( delimterGbn );
        try{
            this.empno = fieldDatas[_offset];
            _offset ++;
            this.ename = fieldDatas[_offset];
            _offset ++;
            this.job = fieldDatas[_offset];
            _offset ++;
            this.mgr = fieldDatas[_offset];
            _offset ++;
            this.hiredate = fieldDatas[_offset];
            _offset ++;
            this.sal = com.inswave.elfw.util.TypeConversionUtil.parseToLong( fieldDatas[_offset] );
            _offset ++;
            this.comm = com.inswave.elfw.util.TypeConversionUtil.parseToDouble( fieldDatas[_offset] );
            _offset ++;
            this.account = fieldDatas[_offset];
            _offset ++;
            this.deptno = fieldDatas[_offset];
            _offset ++;
        }catch(Exception e) {
            AppLog.error("unMarshalDelimeter Error:["+ lineStr+"]", e);
            throw e;
        }
    }

    @Override
    public void _xStreamEnc() {
    }


    @Override
    public void _xStreamDec() {
    }


}
