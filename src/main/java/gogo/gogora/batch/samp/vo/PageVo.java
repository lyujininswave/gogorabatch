package gogo.gogora.batch.samp.vo;

import com.inswave.elfw.annotation.ElDto;
import com.inswave.elfw.annotation.ElDtoField;
import com.fasterxml.jackson.annotation.JsonFilter;

@JsonFilter("elExcludeFilter")
@ElDto(FldYn = "", delimeterYn = "", logicalName = "페이지 VO")
public class PageVo extends com.inswave.elfw.core.BatchVO {

    public PageVo(){
    }

    @ElDtoField(logicalName = "최소Row", physicalName = "minRow", type = "long", typeKind = "", fldYn = "", delimeterYn = "", cryptoGbn = "", length = 0, dotLen = 0, baseValue = "", desc = "")
    private long minRow;

    @ElDtoField(logicalName = "초대Row", physicalName = "maxRow", type = "long", typeKind = "", fldYn = "", delimeterYn = "", cryptoGbn = "", length = 0, dotLen = 0, baseValue = "", desc = "")
    private long maxRow;

    public long getMinRow(){
        return minRow;
    }

    public void setMinRow(long minRow){
        this.minRow = minRow;
    }

    public long getMaxRow(){
        return maxRow;
    }

    public void setMaxRow(long maxRow){
        this.maxRow = maxRow;
    }

    @Override
    public String toString() {
        return "PageVo [minRow=" + minRow + ",maxRow=" + maxRow + "]";
    }

    public boolean isFixedLengthVo() {
        return false;
    }

    @Override
    public void _xStreamEnc() {
    }


    @Override
    public void _xStreamDec() {
    }


}
