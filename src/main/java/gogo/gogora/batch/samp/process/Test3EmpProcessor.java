package gogo.gogora.batch.samp.process;

import javax.annotation.Resource;

import org.springframework.batch.item.ItemProcessor;

import gogo.gogora.batch.samp.dao.Emp2DAO;
import gogo.gogora.batch.samp.vo.EmpHeadVo;
import gogo.gogora.batch.samp.vo.EmpTailVo;
import gogo.gogora.batch.samp.vo.EmpVo;

import com.inswave.elfw.core.CommVO;
import com.inswave.elfw.log.AppLog;

public class Test3EmpProcessor implements ItemProcessor<CommVO, CommVO> {
  
    @Resource(name="emp2DAO") 
    private Emp2DAO emp2DAO;

	@Override
	public CommVO process(CommVO item) throws Exception {
		if (item instanceof EmpVo) {
			EmpVo empVo = (EmpVo) (item.clone());
			empVo.setAccount("[APPEND]" + empVo.getAccount());
			AppLog.debug(" - Data process :: " + empVo);
			emp2DAO.insertEmp(empVo);
			
			return empVo;
		} else if (item instanceof EmpHeadVo) {
			EmpHeadVo empHeadVo = (EmpHeadVo) (item.clone());

			AppLog.debug("- Head[" + item.getClass().getName() + "]:" + empHeadVo.toString());
			return null;
		} else if (item instanceof EmpTailVo) {
			EmpTailVo empTailVo = (EmpTailVo) (item.clone());
			// TODO: Biz ...

			AppLog.debug("- Tail[" + item.getClass().getName() + "]:" + empTailVo.toString());
			return null;
		}

		return null;
	}
}
