package gogo.gogora.batch.samp.process;


import org.springframework.batch.item.ItemProcessor;

import com.inswave.elfw.core.CommVO;
import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.vo.EmpVo;


public class Test4EmpProcessor implements ItemProcessor<CommVO, CommVO> {
 
	@Override
	public CommVO process(CommVO item) throws Exception {

		if (item instanceof EmpVo) {
			EmpVo empVo = (EmpVo)item;
			AppLog.debug("- process ["+empVo.getEmpno()+"]==>" + empVo);
		}
		
		return null;
	}
}
