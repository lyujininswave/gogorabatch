package gogo.gogora.batch.samp.process;

import org.springframework.batch.item.ItemProcessor;

import com.inswave.elfw.core.CommVO;
import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.vo.EmpHeadVo;
import gogo.gogora.batch.samp.vo.EmpTailVo;
import gogo.gogora.batch.samp.vo.EmpVo;

public class TestEmpProcessor implements ItemProcessor<CommVO, CommVO> {
   
    private String paramEname;
    private long loCur = 0;
	public void setParamEname(String paramEname) {
		this.paramEname = paramEname;
	}
 
	@Override
	public CommVO process(CommVO item) throws Exception {

		
		if( item instanceof EmpVo ) {
			EmpVo empVo = (EmpVo)(item.clone());
			empVo.setAccount("[APPEND]" + empVo.getAccount() );
			AppLog.debug(" - Data process :: " + empVo );
			return empVo;
		} else if( item instanceof EmpHeadVo ) {
			EmpHeadVo empHeadVo = (EmpHeadVo)(item.clone());
			return empHeadVo;
		} else if(item instanceof EmpTailVo){
			EmpTailVo empTailVo = (EmpTailVo)(item.clone());
			return empTailVo;
		}
		
		return null;
	}
}
