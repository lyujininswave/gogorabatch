package gogo.gogora.batch.samp.reader;

import com.inswave.elfw.batch.reader.ElCursorItemReader;
import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.vo.EmpVo;

public class TestEmpCursorItemReader extends ElCursorItemReader<EmpVo> {
	
    private String paramEname;
    
	public void setParamEname(String paramEname) {
		this.paramEname = paramEname;
	}
	
	@Override
	protected void doOpen() throws Exception {
	
		// 필수
		super.doOpen();   

		// 로직 추가 가능
		String queryId = "gogo.gogora.batch.samp.selectEmpList";
		
		EmpVo parameterVo = new EmpVo();
		parameterVo.setEname(paramEname);
				
		// 최종적으로 해당 Query를 커서로 해서 Process 에 전달함 
		setSelectCursor(queryId, parameterVo);
		
		AppLog.debug(" - doOpen End");

	}
	
}
