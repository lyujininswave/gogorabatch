package gogo.gogora.batch.samp.reader;


import javax.annotation.Resource;


import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.service.LocalEmpService;
import gogo.gogora.batch.samp.vo.EmpVo;


public class TestCustItemReader implements ItemReader<EmpVo> {
  
	 
    @Resource(name = "localEmpService")
    private LocalEmpService localEmpService;	
	
	private int iIndex = 0;
	
    @Override
	public EmpVo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		// 로직 추가 가능
		EmpVo empVo = localEmpService.getRemoteReceiveData(); 
		AppLog.debug("- read ["+iIndex+"]==>" + empVo);
			
		iIndex++;		
		
		if( empVo != null ) {
			return empVo;
		} else { 
			return null;  // read 종료 시점 
		}

	}
		
}
