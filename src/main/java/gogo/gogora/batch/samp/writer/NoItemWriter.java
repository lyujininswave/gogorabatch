package gogo.gogora.batch.samp.writer;

import java.util.List;


import org.springframework.batch.item.ItemWriter;

import com.inswave.elfw.core.CommVO;
import com.inswave.elfw.log.AppLog;

import gogo.gogora.batch.samp.vo.EmpHeadVo;
import gogo.gogora.batch.samp.vo.EmpTailVo;
import gogo.gogora.batch.samp.vo.EmpVo;

public class NoItemWriter implements ItemWriter<Object>{
   
	@Override
	public void write(List<? extends Object> arg0) throws Exception {
		
	}
}
