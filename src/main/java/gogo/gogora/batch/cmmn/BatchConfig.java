package gogo.gogora.batch.cmmn;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatchConfig {

	@Bean
	public static BeanFactoryPostProcessor beanFractoryPostProcessor(){
	
		BeanFactoryPostProcessor beanFactory = new BatchBeanFactoryPostProcessor();
		return beanFactory;
	}

}
