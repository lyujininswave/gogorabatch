package gogo.gogora.batch.cmmn;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;

import com.inswave.elfw.db.ElAbstractMybatisDAO;

/**   
 * @Class Name : GogoraDefaultAbstractDAO.java
 * @Description : 프로젝트 Default DAO 상위 클래스 - 각 업무 DAO 에서 상속받아 사용됨(기본 데이터 소스 사용시) 
 * @Modification Information  
 * @
 * @  수정일                  수정자                  수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2019.08.01   샘플개발팀                  최초생성
 * 
 * @author EL개발팀
 * @since 2019.08.01
 * @version 1.0
 * @see
 * 
 *  Copyright Inswave (C) by Sampler All right reserved.
 */
public class GogoraDefaultAbstractDAO extends ElAbstractMybatisDAO{


	 @Resource(name = "sqlSessionFactory")
	 public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	 }
	
	 @Resource(name = "sqlSessionTemplate")
	 public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	    super.setSqlSessionTemplate(sqlSessionTemplate);
	 }
}
