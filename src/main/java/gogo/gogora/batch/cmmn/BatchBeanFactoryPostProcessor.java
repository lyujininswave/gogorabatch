package gogo.gogora.batch.cmmn;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**   
 * @Class Name : GogoraDefaultAbstractDAO.java
 * @Description : 프로젝트 Default DAO 상위 클래스 - 각 업무 DAO 에서 상속받아 사용됨(기본 데이터 소스 사용시) 
 * @Modification Information  
 * @
 * @  수정일                  수정자                  수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2019.08.01   샘플개발팀                  최초생성
 * 
 * @author EL개발팀
 * @since 2019.08.01
 * @version 1.0
 * @see
 * 
 *  Copyright Inswave (C) by Sampler All right reserved.
 */
public class BatchBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		for(String beanName : beanFactory.getBeanDefinitionNames()){
			beanFactory.getBeanDefinition(beanName).setLazyInit(true);
		}
	}

}
