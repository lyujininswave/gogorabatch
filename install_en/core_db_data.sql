/*
What to modify when changing Windows/Linux
- Applications > JAVA_HOME
- Applications > APPLICATION_LOG_DIR 
*/


/*
DELETE FROM EL_APP   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_SVC_MENU WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_APP_CMT_DEPL   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_APP_CMT_DEPL_PARAM   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_RSC   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_NOTIFY  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_AUTH  WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_SVC_CTR  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_GROUP_TREE  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_GROUP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM  EL_SVC  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SYS_PROP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_MENU_AUTH  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_USER  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_USER_GROUP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_MENU  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_MENU_GROUP  WHERE APP_ID = 'GogoraBatch';

commit;

 */


/* EL_APP information below needs to be modified according to the system situation */  
Insert into EL_APP
   (APP_ID, APP_NAME, APP_ORDER, JAVA_HOME, JAVAC_OPTION, 
    APP_JAR_DIR, APP_CLASS_PATH)
 Values
   ('GogoraBatch', 'GogoraBatch', 100, 'C:/InswaveToolSP1/tools/openjdk-1.8.0.292-windows.x86_64', '-encoding UTF-8 -g',  
    'C:/TEMP/Lib_1;C:/TEMP/Lib_2', 'C:/TEMP/Deploy_test_dir/classes');
    

/* EL_APP_CMT_DEEPL information below needs to be modified to suit the system situation - at least one basic copy distribution is required */
Insert into EL_APP_CMT_DEPL
   (APP_ID, APP_DEPL_ID, APP_DEPL_GBN, APP_DEPL_NAME, APP_DEPL_CLASS, 
    APP_DEPL_YN, DEPL_SRC_PATH, DEPL_RES_PATH, DEPL_CLS_PATH, DEPL_WEB_PATH)
 Values
   ('GogoraBatch', 'GogoraBatch_node1', 'DEV', 'GogoraBatch_Basic Node', 'com.inswave.elfw.deploy.DefaultDeployCopyAdapter', 
    'Y', 'C:/TEMP/Deploy_test_batch_dir/src', 'C:/TEMP/Deploy_test_batch_dir/res', 'C:/TEMP/Deploy_test_dir/classes', 'C:/TEMP/Deploy_test_batch_dir/web');
    
   
COMMIT;

Insert into EL_NOTIFY
   (APP_ID, SERVER_NO, NOTIFY_BASE_URL, USED_YN)
 Values
   ('GogoraBatch', 1, 'null', 'Y');



Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SERVER_MODE', 'Server mode', 'DEV', 'Server mode ( DEV, RUN)', 100);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_FACTORY_IMPL', 'App Log Implementation Class', 'com.inswave.elfw.log.DefaultApplicationLogFactoryImpl', 'Log Factory Implementation (customizable)');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_DIR', 'Save App Log to', 'C:/InswaveToolSP1/logs/GogoraBatch', 'Location of log files', 210);
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_SEND_TARGET_LOGGER_NAME', 'Name to send outside App log', 'elfw.appLogger', 'Name to send AppLog to external Appender - Do not send when it does not exist');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_APPEND_LOGGERS', 'External logger name to add to APP log', 'External logger to be imported (separated by,) - specifies the level after the logger if the setting of the external logger is forced to be changed. ex) java.sql:ERROR');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'LOG4J_LOADING_BASE_FILE', 'Log4j file name to load', 'log4j.xml', 'Log4xml to load - Must be immediately in class path');
   
   
   
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_LEVEL', 'App log level', 'DEBUG', 'Log level (set app log only) - For external loggers, follow the settings of external loggers', 230);


Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SQL_LOG_LINE_FEED_YN', 'SQL Log Wrap use Status', 'Y', 'SQL Log Wrap Status', 250);
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SQL_WRITE_LOG_LEVEL', 'Log level at which to write SQL', 'OFF', 'Log level to log SQL statements (DEBUG, OFF) - Default is DEBUG', 240);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC,SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_SIZE', 'App log size', '100M', 'Log File Size',245);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, 
    SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'DEFAULT_LOG_FORMATTER_NAME', 'Log formatter implementation class name', 'com.inswave.elfw.log.DefaultAppFormatter', 
    'Log Formatter Implementation Class Name - AppLogFormatter Extended Implementation');

/* Add 20191204 batch parameter DB-based control is added for each project. */
Insert into EL_SYS_PROP 
   (APP_ID ,SYS_GROUP_ID ,SYS_KEY ,SYS_SUB ,SYS_VAL ,SYS_DESC ,SORT_SEQ)  
 Values 
   ('GogoraBatch', 'EL_CORE_PROP', 'EL_BATCH_DB_PARAM_MODE_YN', 'Batch Parameter DB use Status', 'Y', 
    'Batch Parameter DB use Status', 300);

Insert into EL_SYS_PROP 
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'EL_BATCH_PARAM_VALUE_CLASS', 'Batch Parameter Value Implementation Class Name', '', 
    'Batch Parameter Value Implementation Class Name', 310); 
    
COMMIT;

