/*
윈도우/리눅스 변경 시 바꿔야할 부분
- 어플리케이션관리 > JAVA_HOME
- 프로퍼티관리 > APPLICATION_LOG_DIR 
*/


/*
DELETE FROM EL_APP   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_SVC_MENU WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_APP_CMT_DEPL   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_APP_CMT_DEPL_PARAM   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_RSC   WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_NOTIFY  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_AUTH  WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_SVC_CTR  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_GROUP_TREE  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SVC_GROUP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM  EL_SVC  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_SYS_PROP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_MENU_AUTH  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_USER  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_USER_GROUP  WHERE APP_ID = 'GogoraBatch';

DELETE FROM EL_MENU  WHERE APP_ID = 'GogoraBatch';

DELETE FROM	EL_MENU_GROUP  WHERE APP_ID = 'GogoraBatch';

commit;

 */


/* 아래 EL_APP 정보는 시스템 상황에 맞게 수정 필요  */  
Insert into EL_APP
   (APP_ID, APP_NAME, APP_ORDER, JAVA_HOME, JAVAC_OPTION, 
    APP_JAR_DIR, APP_CLASS_PATH)
 Values
   ('GogoraBatch', 'GogoraBatch', 100, 'C:/InswaveToolSP1/tools/openjdk-1.8.0.292-windows.x86_64', '-encoding UTF-8 -g', 
    'C:/TEMP/Lib_1;C:/TEMP/Lib_2', 'C:/TEMP/Deploy_test_dir/classes');
    

/* 아래 EL_APP_CMT_DEPL 정보는 시스템 상황에 맞게 수정 필요 - 최소 1개의 기본 Copy 배포는 필요함   */  
Insert into EL_APP_CMT_DEPL
   (APP_ID, APP_DEPL_ID, APP_DEPL_GBN, APP_DEPL_NAME, APP_DEPL_CLASS, 
    APP_DEPL_YN, DEPL_SRC_PATH, DEPL_RES_PATH, DEPL_CLS_PATH, DEPL_WEB_PATH)
 Values
   ('GogoraBatch', 'GogoraBatch_node1', 'DEV', 'GogoraBatch_기본노드', 'com.inswave.elfw.deploy.DefaultDeployCopyAdapter', 
    'Y', 'C:/TEMP/Deploy_test_batch_dir/src', 'C:/TEMP/Deploy_test_batch_dir/res', 'C:/TEMP/Deploy_test_dir/classes', 'C:/TEMP/Deploy_test_batch_dir/web');
    
   
COMMIT;

Insert into EL_NOTIFY
   (APP_ID, SERVER_NO, NOTIFY_BASE_URL, USED_YN)
 Values
   ('GogoraBatch', 1, 'null', 'Y');



Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SERVER_MODE', '서버 모드', 'DEV', '서버 모드 ( DEV, RUN ).', 100);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_FACTORY_IMPL', 'App 로그 구현체 클래스 ', 'com.inswave.elfw.log.DefaultApplicationLogFactoryImpl', 'Log Factory 구현체 ( 커스트 마이징 가능 )');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_DIR', 'App 로그 저장위치', '/log/proworks/GogoraBatch', '로그 파일의 위치', 210);
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_SEND_TARGET_LOGGER_NAME', 'App 로그 외부로 보낼 이름', 'elfw.appLogger', 'AppLog를 외부 Appender 로 보낼 이름 - 존재하지 않을시에 보내지 않음');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_APPEND_LOGGERS', 'APP 로그에 추가할 외부로거 이름', '가져올 외부로거 (,로 구분)- 외부 로거의 설정을 강제로 변경할 경우 로거 뒤에 레벨을 명시함. ex) java.sql:ERROR');
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'LOG4J_LOADING_BASE_FILE', '로딩할 log4j 파일명 ', 'log4j.xml', '로딩할 log4.xml - 클래스 패스에 바로 존재하여야 함');
   
   
   
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_LEVEL', 'App 로그레벨', 'DEBUG', '로그레벨 ( app 로그만 설정 ) - 외부 로거의 경우 외부 로거의 설정을 따라간다.', 230);


Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SQL_LOG_LINE_FEED_YN', 'SQL 로그 줄바꿈 사용여부', 'Y', 'SQL로그 줄바꿈 처리여부', 250);
Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'SQL_WRITE_LOG_LEVEL', 'SQL 기록할 로그레벨', 'OFF', 'SQL문 기록할 로그레벨( DEBUG, OFF ) - 기본은 DEBUG', 240);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC,SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'APPLICATION_LOG_SIZE', 'App 로그크기', '100M', '로그파일크기',245);

Insert into EL_SYS_PROP
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, 
    SYS_DESC)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'DEFAULT_LOG_FORMATTER_NAME', '로그 포멧터 구현 클래스명', 'com.inswave.elfw.log.DefaultAppFormatter', 
    '로그 포멧터 구현 클래스명 - AppLogFormatter 상속받아 구현 ');

/* Add 20191204 배치 파라미터 DB 기반 제어할 프로젝트 별로 추가한다. */
Insert into EL_SYS_PROP 
   (APP_ID ,SYS_GROUP_ID ,SYS_KEY ,SYS_SUB ,SYS_VAL ,SYS_DESC ,SORT_SEQ)  
 Values 
   ('GogoraBatch', 'EL_CORE_PROP', 'EL_BATCH_DB_PARAM_MODE_YN', '배치 파라미터 DB 사용 여부', 'Y', 
    '배치 파라미터 DB 사용 여부', 300);

Insert into EL_SYS_PROP 
   (APP_ID, SYS_GROUP_ID, SYS_KEY, SYS_SUB, SYS_VAL, SYS_DESC, SORT_SEQ)
 Values
   ('GogoraBatch', 'EL_CORE_PROP', 'EL_BATCH_PARAM_VALUE_CLASS', '배치 파라미터 값 구현 클래스명', '', 
    '배치 파라미터 값 구현 클래스명', 310); 
    
COMMIT;

