@echo off


set JAVA_HOME=C:/InswaveToolSP1/tools/openjdk-1.8.0.292-windows.x86_64
set INSWAVE_CORE_LIB=C:\InswaveToolSP1\lib\coreBatch

@rem ################# ClassPath ###########################
set CLASSPATH=C:/InswaveToolSP1/workspace/GogoraBatch/target/classes

set CLASSPATH=%CLASSPATH%;C:\InswaveToolSP1\workspace\GogoraBatch\lib/*;%INSWAVE_CORE_LIB%/*

IF "%SQL_MAP_XML%" == "" (
	set SQL_MAP_XML=context-batch-sqlMap.xml
)

IF "%COMPONENT_SCAN_PKG%" == "" (
	set COMPONENT_SCAN_PKG=gogo.gogora.batch
)

%JAVA_HOME%/bin/java %JVM_PARAM% %JVM_MEMORY% -DSQL_PKG=%SQL_PKG% -DCOMPONENT_SCAN_PKG=%COMPONENT_SCAN_PKG% -cp %CLASSPATH% com.inswave.elfw.batch.ElDefaultCommandLineJobLauncher %RUN_JOB_LOAD_XML% %RUN_JOB_ID% %*

echo %ERRORLEVEL%

IF  %ERRORLEVEL% == 0 (
	echo %RUN_JOB_ID% : Sucess
@rem    exit %ERRORLEVEL%
) ELSE (
    echo %RUN_JOB_ID% : Error
@rem    exit %ERRORLEVEL%
)


