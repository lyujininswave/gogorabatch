#!/usr/sh
# ==================================================================================================
# @file             inswaveInvokeBatch.sh
#
# @dep-program
#
# @dep-infile
# @dep-outfile
# 
# @usage
#
# @history
#    VERSION : NAME     : DATE     : BASED ON        : HISTORY DATA
#    -------   --------   --------   ---------------   -------------------------------------------
#    VER1.00 : 공�?��??     : 2019/08/01 :                 : ??�?????
#
# ==================================================================================================
# **************************************** env definition ******************************************
 
PW_BATCH_CONSOL_LOG()
{
    #echo "" $@
    echo "" $@ >> ${BATCH_LOG_NAME} 2>&1
}

umask 000

# ------------------------------------------------------------------------------------------------ #
#  Proworks 배�????? ?��?��???? ClassPath 
# ------------------------------------------------------------------------------------------------ #

export CLASSPATH="/home2/jeus7/proworks5/batch/GogoraBatch/classes"
export CLASSPATH="${CLASSPATH}:/home2/jeus7/proworks5/batch/GogoraBatch/res"
export INSWAVE_CORE_LIB="/home2/jeus7/proworks5/lib/coreBatchLib"
export INSWAVE_EXT_LIB="/home2/jeus7/proworks5/lib/extLib"

for coreLib in ${INSWAVE_CORE_LIB}/*.jar
do
        export CLASSPATH="${CLASSPATH}:$coreLib"
done

for extLib in ${INSWAVE_EXT_LIB}/*.jar
do
        export CLASSPATH="${CLASSPATH}:$extLib"
done

# ------------------------------------------------------------------------------------------------ #
#  JVM �?모리 ?��??
# ------------------------------------------------------------------------------------------------ #
if [ "${JVM_MEMORY}" = "" ] ; then
   export JVM_MEMORY="-Xms64M -Xmx128M"
fi  

# ------------------------------------------------------------------------------------------------ #
#  SQL MAP ???? ?��?? 
# ------------------------------------------------------------------------------------------------ #
if [ "${SQL_MAP_XML}" = "" ] ; then
   export SQL_MAP_XML="context-batch-sqlMap.xml"
fi 

# ------------------------------------------------------------------------------------------------ #
#  Java Component Scan ?? BasePackage ?��?? 
# ------------------------------------------------------------------------------------------------ #
if [ "${COMPONENT_SCAN_PKG}" = "" ] ; then
   export COMPONENT_SCAN_PKG="gogo.gogora.batch"
fi  

# ------------------------------------------------------------------------------------------------ #
#  JVM ???��?��?? ?��??
# ------------------------------------------------------------------------------------------------ #
export JVM_PARAM="${JVM_PARAM} -Dinswave.batch.key=inswave"

# ------------------------------------------------------------------------------------------------ #
#  ???��?? ??�?�? Process ???? ( 미�????? ??�?리�?? Skip ?? )
# ------------------------------------------------------------------------------------------------ #
export SYS_PRE_POST_PROCESS=com.inswave.elfw.batch.process.DummyElBatchJobSysProcess

# ------------------------------------------------------------------------------------------------ #
#  Inswave 배�????? ?��?��????  ??경�??? 
# ------------------------------------------------------------------------------------------------ #
export JAVA_HOME=/usr/local/java/jdk1.7.0_80

${JAVA_HOME}/bin/java ${JVM_PARAM} ${JVM_MEMORY} -cp "${CLASSPATH}" com.inswave.elfw.batch.ElDefaultCommandLineJobLauncher ${RUN_JOB_LOAD_XML} ${RUN_JOB_ID} $*

export retVal=$?

if [ ${retVal} -eq 0 ]; then
	echo ${RUN_JOB_ID} ': Sucess'
    exit ${retVal}
else 
    echo ${RUN_JOB_ID} ': Error'    
    exit ${retVal}
fi  
